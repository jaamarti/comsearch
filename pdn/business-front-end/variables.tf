# Module: business-front-end

variable "subnets" {
  default = ["VPC-DEMO-COMSEARCH-PDN-public-us-west-2a", "VPC-DEMO-COMSEARCH-PDN-public-us-west-2b", "VPC-DEMO-COMSEARCH-PDN-public-us-west-2c"]
  type    = list(any)
}

variable "vpc_name" {
  default = ["VPC-DEMO-COMSEARCH-PDN"]
  type    = list(any)
}
