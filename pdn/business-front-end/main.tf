# Module: business-front-end


terraform {
  backend "s3" {
    bucket = "cf-templates-1bdxdp30rbh5h-us-west-2"
    key    = "pdn/businessfned/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = var.vpc_name
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = var.subnets
  }
}

module "front-end" {
  source        = "../../modules/elb-asg-ec2"
  aws_region    = "us-west-2"
  env           = "pdn"
  elb_name      = "comseach-frontend-demo-pdn"
  asg_max       = "4"
  asg_min       = "3"
  asg_desired   = "3"
  subnets       = data.aws_subnet_ids.selected.ids
  vpcid         = data.aws_vpc.selected.id
  instance_type = "t2.micro"
  userdata      = "../../modules/elb-asg-ec2/userdata.sh"
}

