# Module: business-front-end


terraform {
  backend "s3" {
    bucket = "cf-templates-1bdxdp30rbh5h-us-west-2"
    key    = "pdn/networking/terraform.tfstate"
    region = "us-west-2"
  }
}

provider "aws" {
  region = "us-west-2"
}

module "vpc" {

  ## Terraform Managed module ##
  source = "terraform-aws-modules/vpc/aws"

  ## Git module ##
  # source = "git::ssh://bitbucketURL/my_repo.git.git?ref=BranchName"

  ## Custom module ##
  # source = "../../global/vpc"

  name                = "VPC-DEMO-COMSEARCH-PDN"
  cidr                = "10.0.0.0/16"
  azs                 = ["us-west-2a", "us-west-2b", "us-west-2c"]
  private_subnets     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets      = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  database_subnets    = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
  elasticache_subnets = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
  redshift_subnets    = ["10.0.13.0/24", "10.0.14.0/24", "10.0.15.0/24"]
  intra_subnets       = ["10.0.16.0/24", "10.0.17.0/24", "10.0.18.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform   = "true"
    Environment = "pdn"
    Description = "vpc Demo"
  }

  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60
}


