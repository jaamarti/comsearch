# Module: ec2-demo

provider "aws" {
  region = var.aws_region
}

resource "aws_elb" "web-elb" {
  name = var.elb_name
  # The same availability zone as our instances
  subnets = var.subnets
  #availability_zones = "${split(",", var.availability_zones)}"
  security_groups = ["${aws_security_group.default.id}"]
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
}
resource "aws_autoscaling_group" "web-asg" {
  #availability_zones   = "${split(",", var.availability_zones)}"
  name                 = "frontend AG"
  max_size             = var.asg_max
  min_size             = var.asg_min
  desired_capacity     = var.asg_desired
  force_delete         = true
  launch_configuration = aws_launch_configuration.web-lc.name
  load_balancers       = ["${aws_elb.web-elb.name}"]
  vpc_zone_identifier  = var.subnets

  #  tag {
  #    key                 = "Name"
  #    value               = "web-asg"
  #    propagate_at_launch = "true"
  #  }
  tags = [
    {
      key                 = "env"
      value               = var.env
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "web-asg"
      propagate_at_launch = true
    },
  ]

  lifecycle {
    create_before_destroy = true
  }

}
resource "aws_launch_configuration" "web-lc" {
  name_prefix   = "launch configuration comsearch frontend-"
  image_id      = lookup(var.aws_amis, var.aws_region)
  instance_type = var.instance_type
  # Security group
  security_groups = ["${aws_security_group.default.id}"]
  user_data       = file("${var.userdata}")
  #  vpc_classic_link_id = "vpc-012a610d664dc9305"
  #  key_name        = var.key_name
  lifecycle {
    create_before_destroy = true
  }
}
# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "front end instances sg"
  vpc_id      = var.vpcid
  description = "Used in the terraform"
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
