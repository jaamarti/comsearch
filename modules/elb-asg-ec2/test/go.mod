module elb-asg-ec2

go 1.16

require (
	github.com/gruntwork-io/terratest v0.35.3
	github.com/stretchr/testify v1.7.0
)
