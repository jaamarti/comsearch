aws_ec2_name = input("aws_ec2_name")
aws_ec2_ami = input("aws_ec2_ami")
aws_ec2_env = input("aws_ec2_env")

describe aws_ec2_instance(name: aws_ec2_name) do
    it              { should be_running }
    it              { should_not have_roles }
    its('image_id') { should eq aws_ec2_ami }
    its('tags_hash') { should include('env' => aws_ec2_env) }
end


