# Module: business-front-end

variable "subnets" {
  default = ["VPC-DEMO-COMSEARCH-DEV-public-us-west-1c", "VPC-DEMO-COMSEARCH-DEV-public-us-west-1b"]
  type    = list(any)
}

variable "vpc_name" {
  default = ["VPC-DEMO-COMSEARCH-DEV"]
  type    = list(any)
}
