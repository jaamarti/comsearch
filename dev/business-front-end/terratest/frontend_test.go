package test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"

	"github.com/gruntwork-io/terratest/modules/terraform"
)

func TestTerraformAwsFrontEndTest(t *testing.T) {
	t.Parallel()

	// Construct the terraform options with default retryable errors to handle the most common
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../",
	})

	// At the end of the test, run `terraform destroy` to clean up any resources that were created.
	defer terraform.Destroy(t, terraformOptions)

	// Run `terraform init` and `terraform apply`. Fail the test if there are any errors.
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the IP of the instance
	elbName := terraform.Output(t, terraformOptions, "Front_end_elb_domain")

	// time.Sleep(100 * time.Second)

	// Make an HTTP request to the instance and make sure we get back a 200 OK with the body "Hello, World!"
	url := fmt.Sprintf("http://%s", elbName)
	//http_helper.HttpGetWithRetry(t, url, nil, 200, "*string", 30, 5*time.Second)
	http_helper.HttpGetWithRetryWithCustomValidation(t, url, nil, 6, 25*time.Second, func(statusCode int, body string) bool {
		isOk := statusCode == 200
		isNginx := strings.Contains(body, "COMSEARCH")
		return isOk && isNginx
	})
}