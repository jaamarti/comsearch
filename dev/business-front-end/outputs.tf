# Module: business-front-end

output "security_group" {
  value = module.front-end.security_group
}

output "launch_configuration" {
  value = module.front-end.launch_configuration
}

output "asg_name" {
  value = module.front-end.asg_name
}

output "Front_end_elb_domain" {
  value = module.front-end.elb_name
}

#output "ec2_name" {
#value = "${lookup(module.front-end.tags, "Name")}"
#}
