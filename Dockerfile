# Build an ubuntu image with awscli and terraform support and one Linux account per
# AWS account
#
# 1) Put this Dockerfile in a folder.
# 2) Create a sub-folder called home and sub-folders within that for each Linux
#    account. These sub-folders can contain any files you want added to the
#    Linux account's home directory, e.g. .bash_profile, .aws, etc.
# 3) Build the image:
#    docker build -t andypowe11/ubuntu-awscli .
# 4) Run the container:
#
#    docker run -i -t -h ubuntu-awscli andypowe11/ubuntu-awscli _account_
#
#    or:
#
#    docker run -i -t -v C:/Users/ap/Docker/ubuntu-awscli/home:/home \
#        -h ubuntu-awscli andypowe11/ubuntu-awscli _account_
#
#    if you want to mount the Linux hone directories on the host filesystem.
#    Doing this means that content, e.g. .bash_history, is preserved across
#    sessions.
#
FROM ubuntu
# Create a user for each AWS account
# For each user, copy home directory files from folder with same name as
# the user
RUN sudo apt-get update
RUN sudo apt-get -y install unzip
RUN sudo apt-get -y install wget
RUN wget https://releases.hashicorp.com/terraform/0.15.5/terraform_0.15.5_linux_arm64.zip
RUN unzip terraform_0.15.5_linux_arm64.zip
RUN sudo mv terraform /usr/local/bin/
RUN apt-get install golang-go -y
